$(document).ready(function() {
	var api_link = "https://api.github.com";
	var commit_limit = 30;
	
	// Initial Commit List
	getCommitList();
	
	function getCommitList() {
		$.ajax({
			method: "GET",
			url: api_link + "/repos/angular/angular.js/commits",
			success: function(res){
				var commits = res.slice(0, 30);

				var group_ary = groupBy(commits, function(item) {
				  return [item.author.login];
				});

				var commit_list = "";

				for(var i = 0; i < group_ary.length; i++) {
					commit_list += "<ul>";

					for(var j = 0; j < group_ary[i].length; j++){
						
						var lastOfSha = group_ary[i][j].sha.slice(-1);
						
						if($.isNumeric(lastOfSha))
							commit_list += "<li class='light-blue'>";
						else
							commit_list += "<li>";
						
						commit_list += "<img src='"+group_ary[i][j].author.avatar_url+"' />";
						commit_list += "<ul><li><span class='commit-title'>"+group_ary[i][j].commit.message.split("\n")[0]+"</span></li>";
						commit_list += "<li><span class='author'><b>"+group_ary[i][j].author.login+"</b></span>";
						var commit_time = getTimeDiff(group_ary[i][j].commit.author.date);
						commit_list += "<span class='commit-date'> &nbsp;committed&nbsp;"+commit_time+"</span>";
						commit_list += "<span class='sha'>commit: "+group_ary[i][j].sha+"</span></li></ul>";
						commit_list += "</li>";
					}

					commit_list += "</ul>";

				}

				$(".commit-list").html(commit_list);
			}
		})		
	}

	function groupBy( array , f ) {
		var groups = {};
		array.forEach( function( o ) {
			var group = JSON.stringify( f(o) );
			groups[group] = groups[group] || [];
			groups[group].push( o );  
		});

		return Object.keys(groups).map( function( group ) {
			return groups[group];
		})
	}

	function getTimeDiff(date){
        //--------------------------------------------------------------------------------------------
        // Time Difference from Now to Creation Date
        //--------------------------------------------------------------------------------------------

		var curDate = new Date();
		var commitDate = new Date(date);
		var timeDiff = Math.floor(curDate.getTime() - commitDate.getTime());
		var second = 1000;

		var seconds = Math.floor(timeDiff/second);
		var minutes = Math.floor(seconds/60);
		var hours = Math.floor(minutes/60);
		var days = Math.floor(hours/24);
		var months = Math.floor(days/31);
		var years = Math.floor(months/12);

		if(years != 0)
			return message = years + " years ago";
		else if (months != 0)
			return message = months + " months ago";
		else if (days != 0)
			return message = days + " days ago";
		else if (hours != 0)
			return message = hours + " hours ago";
		else if (minutes != 0)
			return message = minutes + " minutes ago";
		else if (seconds != 0)
			return message = seconds + " seconds ago";

		return message;
	};

	$(".refresh").click(function() {
		$(".commit-list").html('');

		setTimeout(function(){
			getCommitList();
		}, 1000);
	})
})